# OpenML dataset: lsvt

https://www.openml.org/d/1484

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Athanasios Tsanas

**Source**: UCI 

**Please cite**: A. Tsanas, M.A. Little, C. Fox, L.O. Ramig: Objective automatic assessment of rehabilitative speech treatment in Parkinsons disease, IEEE Transactions on Neural Systems and Rehabilitation Engineering, Vol. 22, pp. 181-190, January 2014   

Dataset title

laLSVT Voice Rehabilitation Data Set 

Source:

The dataset was created by Athanasios Tsanas (tsanasthanasis '@' gmail.com) of the University of Oxford.

Abstract: 
126 samples from 14 participants, 309 features. Aim: assess whether voice rehabilitation treatment lead to phonations considered 'acceptable' or 'unacceptable' (binary class classification problem).


Data Set Information:

The original paper demonstrated that it is possible to correctly replicate the experts' binary assessment with approximately 90% accuracy using both 10-fold cross-validation and leave-one-subject-out validation. We experimented with both random forests and support vector machines, using standard approaches for optimizing the SVM's hyperparameters. It will be interesting if researchers can improve on this finding using advanced machine learning tools. 

Details for the dataset can be found on the following paper. 
A. Tsanas, M.A. Little, C. Fox, L.O. Ramig: â€œObjective automatic assessment of rehabilitative speech treatment in Parkinsonâ€™s diseaseâ€, IEEE Transactions on Neural Systems and Rehabilitation Engineering, Vol. 22, pp. 181-190, January 2014 

A freely available preprint is availabe from the first author's website.


Attribute Information:

Each attribute (feature) corresponds to the application of a speech signal processing algorithm which aims to characterise objectively the signal. These algorithms include standard perturbation analysis methods, wavelet-based features, fundamental frequency-based features, and tools used to mine nonlinear time-series. Because of the extensive number of attributes we refer the interested readers to the relevant papers for further details.


Relevant Papers:

The dataset was introduced in: 
A. Tsanas, M.A. Little, C. Fox, L.O. Ramig: â€œObjective automatic assessment of rehabilitative speech treatment in Parkinsonâ€™s diseaseâ€, IEEE Transactions on Neural Systems and Rehabilitation Engineering, Vol. 22, pp. 181-190, January 2014 

Further details about the speech signal processing algorithms can be found in: 

A. Tsanas, Accurate telemonitoring of Parkinsonâ€™s disease symptom severity using nonlinear speech signal processing and statistical machine learning, D.Phil. (Ph.D.) thesis, University of Oxford, UK, 2012 

A. Tsanas, M.A. Little, P.E. McSharry, L.O. Ramig: â€œNonlinear speech analysis algorithms mapped to a standard metric achieve clinically useful quantification of average Parkinsonâ€™s disease symptom severityâ€, Journal of the Royal Society Interface, Vol. 8, pp. 842-855, 2011 

A. Tsanas, M.A. Little, P.E. McSharry, L.O. Ramig: â€œNew nonlinear markers and insights into speech signal degradation for effective tracking of Parkinsonâ€™s disease symptom severityâ€, International Symposium on Nonlinear Theory and its Applications (NOLTA), pp. 457-460, Krakow, Poland, 5-8 September 2010 

Preprints are available on the first author's website.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1484) of an [OpenML dataset](https://www.openml.org/d/1484). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1484/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1484/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1484/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

